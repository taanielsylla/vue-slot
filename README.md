# Vue-Slot

### Sample slot game running on Vue.js framework using PIXI.js
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run your tests
```
npm run test
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).


### General game information
To change game balance you need to press on the balance text field.

The game will execute a random outcome spin if pressed on the Spin button

To execute a predefined outcome spin, just press the "DO MOCK SPIN" button & follow the instructions displayed in the popup.

The paytable will highlight any win combinations at the end of each spin

