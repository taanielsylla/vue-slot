import * as PIXI from "pixi.js"
import {SYMBOL_TYPE} from "../SlotMachine/SlotConfig";

export default class extends PIXI.Container {

    constructor() {
        super();

    }

    showPaylines(stopData) {
        const paylines = [...this.getPaylinesFromResult(stopData)];
        console.log(paylines);

        // todo, show lines on reels
    }

    getPaylinesFromResult(result, winType = false) {

        let lines = [];

        const
            allTopCherry = result.every(reel => reel["top"] && reel["top"] === SYMBOL_TYPE.CHERRY),
            allMiddleCherry = result.every(reel => reel["middle"] && reel["middle"] === SYMBOL_TYPE.CHERRY),
            allBottomCherry = result.every(reel => reel["bottom"] && reel["bottom"] === SYMBOL_TYPE.CHERRY),

            allTopSeven = result.every(reel => reel["top"] && reel["top"] === SYMBOL_TYPE.SEVEN),
            allMiddleSeven = result.every(reel => reel["middle"] && reel["middle"] === SYMBOL_TYPE.SEVEN),
            allBottomSeven = result.every(reel => reel["bottom"] && reel["bottom"] === SYMBOL_TYPE.SEVEN),

            cherrAndSevenOnTopLine = () => {
                let cherryFound = false, sevenFound = false;
                result.forEach(reel => {
                    if (reel["top"] && reel["top"] === SYMBOL_TYPE.CHERRY) cherryFound = true;
                    if (reel["top"] && reel["top"] === SYMBOL_TYPE.SEVEN) sevenFound = true;
                });
                return cherryFound && sevenFound;
            },

            cherrAndSevenOnMiddleLine = () => {
                let cherryFound = false, sevenFound = false;
                result.forEach(reel => {
                    if (reel["middle"] && reel["middle"] === SYMBOL_TYPE.CHERRY) cherryFound = true;
                    if (reel["middle"] && reel["middle"] === SYMBOL_TYPE.SEVEN) sevenFound = true;
                });
                return cherryFound && sevenFound;
            },

            cherrAndSevenOnBottomLine = () => {
                let cherryFound = false, sevenFound = false;
                result.forEach(reel => {
                    if (reel["bottom"] && reel["bottom"] === SYMBOL_TYPE.CHERRY) cherryFound = true;
                    if (reel["bottom"] && reel["bottom"] === SYMBOL_TYPE.SEVEN) sevenFound = true;
                });
                return cherryFound && sevenFound;
            },


            allBar3onTopLine = result.every(reel => reel["top"] && reel["top"] === SYMBOL_TYPE.BARx3),
            allBar3onMiddleLine = result.every(reel => reel["middle"] && reel["middle"] === SYMBOL_TYPE.BARx3),
            allBar3onBottomLine = result.every(reel => reel["bottom"] && reel["bottom"] === SYMBOL_TYPE.BARx3),


            allBar2onTopLine = result.every(reel => reel["top"] && reel["top"] === SYMBOL_TYPE.BARx2),
            allBar2onMiddleLine = result.every(reel => reel["middle"] && reel["middle"] === SYMBOL_TYPE.BARx2),
            allBar2onBottomLine = result.every(reel => reel["bottom"] && reel["bottom"] === SYMBOL_TYPE.BARx2),


            allBarOnTopLine = result.every(reel => reel["top"] && reel["top"] === SYMBOL_TYPE.BAR),
            allBarOnMiddleLine = result.every(reel => reel["middle"] && reel["middle"] === SYMBOL_TYPE.BAR),
            allBarOnBottomLine = result.every(reel => reel["bottom"] && reel["bottom"] === SYMBOL_TYPE.BAR),


            barOnTopLine = result.some(reel => reel["top"] && reel["top"] === SYMBOL_TYPE.BAR),
            barOnMiddleLine = result.some(reel => reel["middle"] && reel["middle"] === SYMBOL_TYPE.BAR),
            barOnBottomLine = result.some(reel => reel["bottom"] && reel["bottom"] === SYMBOL_TYPE.BAR);

        if (!winType) {

            if (allTopCherry) lines.push(1); // 1 - top pay-line
            if (allMiddleCherry) lines.push(2); // 2 - middle pay-line
            if (allBottomCherry) lines.push(3); // 3 - bottom pay-line

            if (allTopSeven) lines.push(1);
            else if (allMiddleSeven) lines.push(2);
            else if (allBottomSeven) lines.push(3);

            if (cherrAndSevenOnTopLine()) lines.push(1);
            else if (cherrAndSevenOnMiddleLine()) lines.push(2);
            else if (cherrAndSevenOnBottomLine()) lines.push(3);

            if (allBar3onTopLine) lines.push(1);
            else if (allBar3onMiddleLine) lines.push(2);
            else if (allBar3onBottomLine) lines.push(3);

            if (allBar2onTopLine) lines.push(1);
            else if (allBar2onMiddleLine) lines.push(2);
            else if (allBar2onBottomLine) lines.push(3);

            if (allBarOnTopLine) lines.push(1);
            else if (allBarOnMiddleLine) lines.push(2);
            else if (allBarOnBottomLine) lines.push(3);

            if (barOnTopLine) lines.push(1);
            else if (barOnMiddleLine) lines.push(2);
            else if (barOnBottomLine) lines.push(3);

            // remove duplicates
            // lines = [...new Set(lines)];
            // console.log(lines);


        } else {

            if (allTopCherry) lines.push(1);
            if (allMiddleCherry) lines.push(2);
            if (allBottomCherry) lines.push(3);

            if (allTopSeven || allMiddleSeven || allBottomSeven) lines.push(4);
            if (cherrAndSevenOnTopLine() || cherrAndSevenOnMiddleLine() || cherrAndSevenOnBottomLine) lines.push(5);

            if (allBar3onTopLine || allBar3onMiddleLine || allBar3onBottomLine) lines.push(6);
            if (allBar2onTopLine || allBar2onMiddleLine || allBar2onBottomLine) lines.push(7);
            if (allBarOnTopLine || allBarOnMiddleLine || allBarOnBottomLine) lines.push(8);

            if (barOnBottomLine || barOnMiddleLine || barOnTopLine) lines.push(9);
        }

        return lines;
    }


    getPaylinesWinValue(paytableWins) {
        let total = 0;

        paytableWins.forEach(line => {

            switch (line) {
                case 1:
                    total += 2000;
                    break;
                case 2:
                    total += 1000;
                    break;
                case 3:
                    total += 4000;
                    break;
                case 4:
                    total += 150;
                    break;
                case 5:
                    total += 75;
                    break;
                case 6:
                    total += 50;
                    break;
                case 7:
                    total += 20;
                    break;
                case 8:
                    total += 10;
                    break;
                case 9:
                    total += 5;
                    break;
            }
        });

        return total;
    }
}