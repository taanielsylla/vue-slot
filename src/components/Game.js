import * as PIXI from 'pixi.js'
import SlotMachine from "./SlotMachine/SlotMachine";
import {STOP_POSITION, SYMBOL_TYPE} from "./SlotMachine/SlotConfig";
import Panel from "./Panel/Panel";
import Paytable from "./Paytable/Paytable";
import Paylines from "./Paylines/Paylines";


export default class Game extends PIXI.Container {
    slotMachine;


    constructor(app) {
        super();

        this.app = app;

        this.initComponents();
        this.initListeners();
    }


    initComponents() {

        // Slot machine

        this.slotMachine = new SlotMachine();
        this.slotMachine.position.set(this.slotMachine.width * .5, 0);
        this.addChild(this.slotMachine);


        // Pay-lines

        this.paylines = new Paylines();
        this.paylines.position.set(0, 0);
        this.addChild(this.paylines);

        // Spin button

        this.gamePanel = new Panel();
        this.gamePanel.position.set(this.slotMachine.x, 300);
        this.addChild(this.gamePanel);

        // Pay table

        this.paytable = new Paytable();
        this.paytable.position.set(this.slotMachine.x, 470);
        this.addChild(this.paytable);

    }


    initListeners() {
        this.gamePanel.on(this.gamePanel.EVENT.SPIN, this.doSpin, this);
        this.gamePanel.on(this.gamePanel.EVENT.SET_MOCK_DATA, this.onSetMockData, this);
        this.slotMachine.on(this.slotMachine.EVENT.SPIN_COMPLETE, this.onSpinComplete, this);
    }


    doSpin() {
        if (!this.validateSpin()) return;
        this.gamePanel.reduceBalance();

        this.doRandomSpin();
    }


    doMockSpin(mockData) {
        if (!this.validateSpin()) return;
        this.gamePanel.reduceBalance();

        if (!mockData)
            mockData = [
                {type: SYMBOL_TYPE.CHERRY, position: STOP_POSITION.BOTTOM}, // reel 1
                {type: SYMBOL_TYPE.BARx2, position: STOP_POSITION.TOP}, // reel 2
                {type: SYMBOL_TYPE.SEVEN, position: STOP_POSITION.MIDDLE}, // reel 3
            ];

        this.slotMachine.spinReels(mockData);
    }


    async doRandomSpin() {
        this.slotMachine.doRandomSpin();
    }


    async doRandomSpins() {
        await new Promise(resolve => setTimeout(resolve, 1000));
        this.slotMachine.doInfiniteSpins();
    }

    validateSpin() {
        if (this.gamePanel.balanceValue <= 0) {
            window.alert("Not enough balance");
            return false;
        }

        if (this.slotMachine.isSpinning) return false;

        return true;
    }


    onSpinComplete(result) {
        this.paylines.showPaylines(result);

        // flash pay-lines
        const paytableWins = this.paylines.getPaylinesFromResult(result, true);
        this.paytable.showWins(paytableWins);

        // set win amount from pay-lines
        const totalPaylineWin = this.paylines.getPaylinesWinValue(paytableWins);
        this.gamePanel.incrementBalance(totalPaylineWin)
    }


    onSetMockData(dataString) {
        const parsed = [];
        dataString.split(",").forEach(reelStr => {
            parsed.push(reelStr.split("-"));

        });

        const result = [];
        parsed.forEach(reel => {
            let symbolType, stopPosition;

            switch (reel[0]) {
                case "0":
                    symbolType = SYMBOL_TYPE.BAR;
                    break;
                case "1":
                    symbolType = SYMBOL_TYPE.BARx2;
                    break;
                case "2":
                    symbolType = SYMBOL_TYPE.BARx3;
                    break;
                case "3":
                    symbolType = SYMBOL_TYPE.CHERRY;
                    break;
                case "4":
                    symbolType = SYMBOL_TYPE.SEVEN;
                    break;
            }

            switch (reel[1]) {
                case "0":
                    stopPosition = STOP_POSITION.TOP;
                    break;
                case "1":
                    stopPosition = STOP_POSITION.MIDDLE;
                    break;
                case "2":
                    stopPosition = STOP_POSITION.BOTTOM;
                    break;
            }

            result.push({type: symbolType, position: stopPosition});
        });

        this.doMockSpin(result);
    }
}
