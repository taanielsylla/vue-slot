import * as PIXI from "pixi.js"
import Button from "../../core/ViewElements/Button";
import AssetLoader from "../../core/AssetLoader/AssetLoader";

export default class Panel extends PIXI.Container {


    EVENT = {
        SPIN: "SPIN",
        SET_MOCK_DATA: "SET_MOCK_DATA",
        CLEAR_MOCK_DATA: "CLEAR_MOCK_DATA",
    };

    constructor() {
        super();

        this.balanceValue = 500;

        this.createPanelBg();
        this.createSpinBtn();
        this.createMockSpinBtn();
        this.createBalance();


        this.addListeners();
    }


    addListeners() {
        this.spinBtn.on(this.spinBtn.EVENT.TAP, this.onSpinTap, this);
    }


    onSpinTap() {
        this.emit(this.EVENT.SPIN);
    }


    createSpinBtn() {
        this.spinBtn = new Button(AssetLoader.resources["Spin"].texture);

        this.addChild(this.spinBtn);
    }


    createMockSpinBtn() {
        this.mockBtn = new PIXI.Graphics();
        this.mockBtn.beginFill(0x4B184C, 1);
        this.mockBtn.drawRoundedRect(0, 0, 110, 55, 10);
        this.mockBtn.position.set(-163, -28);

        const text = new PIXI.Text("DO MOCK\n" + "SPIN", {
            fontFamily: 'Arial',
            fontSize: 14,
            fontWeight: "bold",
            fill: 0xffffff,
            align: 'center'
        });
        text.anchor.set(.5);
        text.position.set(this.mockBtn.width >> 1, this.mockBtn.height >> 1);
        this.mockBtn.addChild(text);

        this.addChild(this.mockBtn);

        this.mockBtn.interactive = this.mockBtn.buttonMode = true;
        this.mockBtn.on("pointertap", () => {
            const data = window.prompt("Enter symbol types & stop positions for the 3 reels.\n" +
                "\n" +
                "Bar = 0\n" +
                "2xBar = 1\n" +
                "3xBar = 2\n" +
                "Cherry = 3\n" +
                "Seven = 4\n" +
                "\n" +
                "Top stop position = 0\n" +
                "Middle stop position = 1\n" +
                "Bottom stop position = 2\n" +
                "\n" +
                "\n" +
                "First set the symbol type, then add a minus symbol and then add the stop position followed by comma.\n" +
                "Example:\n" +
                "1-2,0-0,4-1\n" +
                " ",
                "4-0,4-1,4-2");
            this.emit(this.EVENT.SET_MOCK_DATA, data);
        }, this);
    }


    createPanelBg() {
        this.bg = new PIXI.Graphics();
        this.bg.beginFill(0x47B784, 1);
        this.bg.drawRoundedRect(0, 0, 350, 130, 20);
        this.bg.pivot.set(175, 45);

        this.addChild(this.bg);

        this.balanceBg = new PIXI.Graphics();
        this.balanceBg.beginFill(0x232323, 1);
        this.balanceBg.drawRoundedRect(0, 0, 290, 40, 20);
        this.balanceBg.pivot.set(145, -40);

        this.balanceBg.interactive = this.balanceBg.buttonMode = true;
        this.balanceBg.on("pointertap", () => {
            const answer = window.prompt("Set balance size");
            this.setBalance(parseInt(answer));
        }, this);

        this.addChild(this.balanceBg);
    }

    createBalance() {
        this.balance = new PIXI.Text(this.balanceValue.toString(), {
            fontFamily: 'Arial',
            fontSize: 24,
            fill: 0xffffff,
            align: 'center'
        });
        this.balance.interactive = false;
        this.balance.anchor.set(.5);
        this.balance.y = 62;
        this.addChild(this.balance);
    }

    setBalance(value) {
        this.balanceValue = value;
        this.balance.text = this.balanceValue.toString();
    }

    incrementBalance(value) {
        this.balanceValue += value;
        this.balance.text = this.balanceValue.toString();
    }

    reduceBalance() {
        this.balanceValue -= 1;
        this.balance.text = this.balanceValue.toString();
    }
}
