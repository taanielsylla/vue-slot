import * as PIXI from "pixi.js"
import PaytableConfig from "./PaytableConfig";
import {SYMBOL_TYPE} from "../SlotMachine/SlotConfig";
import AssetLoader from "../../core/AssetLoader/AssetLoader";

export default class Paytable extends PIXI.Container {

    constructor() {
        super();

        this.createHighlighs();
        this.createText();
        this.addSymbols();
    }

    createText() {
        this.paylinesText = new PIXI.Text(PaytableConfig.text, PaytableConfig.style);
        this.paylinesText.anchor.set(.5);
        this.addChild(this.paylinesText);
    }

    addSymbols() {
        let symbol;

        symbol = this.addChild(this.getSym(SYMBOL_TYPE.CHERRY));
        symbol.position.set(-57, -73);

        symbol = this.addChild(this.getSym(SYMBOL_TYPE.CHERRY));
        symbol.position.set(-66, -56);

        symbol = this.addChild(this.getSym(SYMBOL_TYPE.CHERRY));
        symbol.position.set(-67, -41);

        symbol = this.addChild(this.getSym(SYMBOL_TYPE.SEVEN));
        symbol.width = symbol.height = 15;
        symbol.position.set(-50, -23);


        symbol = this.addChild(this.getSym(SYMBOL_TYPE.CHERRY));
        symbol.position.set(-63, -9);


        symbol = this.addChild(this.getSym(SYMBOL_TYPE.SEVEN));
        symbol.width = symbol.height = 15;
        symbol.position.set(-17, -6);


        symbol = this.addChild(this.getSym(SYMBOL_TYPE.BARx3));
        symbol.position.set(-50, 7);

        symbol = this.addChild(this.getSym(SYMBOL_TYPE.BARx2));
        symbol.width = symbol.height = 15;
        symbol.position.set(-48, 26);


        symbol = this.addChild(this.getSym(SYMBOL_TYPE.BAR));
        symbol.position.set(-50, 39);

        symbol = this.addChild(this.getSym(SYMBOL_TYPE.BAR));
        symbol.position.set(-38, 56);

    }

    getSym(type) {
        const symbol = new PIXI.Sprite(AssetLoader.resources[type].texture);
        symbol.width = symbol.height = 20;
        return symbol;
    }

    createHighlighs() {

        this.highlights = [];

        [...Array(9)].forEach((i, index) => {
            this.highlights[index] = new PIXI.Sprite(PIXI.Texture.WHITE);
            const highlight = this.highlights[index];
            highlight.alpha = 0;
            highlight.width = 200;
            highlight.height = 16;
            highlight.position.set(0, (index * 16) - 64);
            highlight.anchor.set(.5);
            this.addChild(highlight);
        })
    }

    showWins(winArr = []) {
        winArr.forEach(nr => {
            this.blinkEntry(nr);
        })

    }

    async blinkEntry(nr) {
        const highlight = this.highlights[nr - 1];

        highlight.alpha = .5;
        await new Promise(resolve => setTimeout(resolve, 500));
        highlight.alpha = 0;

        await new Promise(resolve => setTimeout(resolve, 500));
        highlight.alpha = .5;
        await new Promise(resolve => setTimeout(resolve, 500));
        highlight.alpha = 0;

        await new Promise(resolve => setTimeout(resolve, 500));
        highlight.alpha = .5;
        await new Promise(resolve => setTimeout(resolve, 500));
        highlight.alpha = 0;

    }
}