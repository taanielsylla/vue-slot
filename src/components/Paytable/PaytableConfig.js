export default {
    text: "3     on top line 2000\n" +
        "3     on center line 1000\n" +
        "3     on bottom line 4000\n" +
        "3     on any line 150\n" +
        "Any     and     on any line 75\n" +
        "3     on any line 50\n" +
        "3     on any line 20\n" +
        "3     on any line 10\n" +
        "Any     on any line 5",

    style: {
        fontFamily: 'Arial',
        fontSize: 14,
        fill: 0xffffff,
        align: 'center'
    }

}