import * as PIXI from 'pixi.js'
import Reel from './Reel';


const REEL_START_DELAY = 250;


export default class SlotMachine extends PIXI.Container {

    reels = [];

    EVENT = {
        SPIN_COMPLETE: "SPIN_COMPLETE",
    };


    constructor() {
        super();

        this.isSpinning = false;


        [...Array(3)].forEach((e, i) => {
            let reel = new Reel(i);
            reel.position.set((i * reel.width), 0);
            this.addChild(reel);
            this.reels.push(reel);
        });

        this.applyMask();


        this.pivot.set(this.width * .5, -20);
    }


    applyMask() {
        const slotMask = new PIXI.Sprite(PIXI.Texture.WHITE);
        slotMask.width = 400;
        slotMask.height = 250;
        slotMask.y = -15;
        this.addChild(slotMask);

        this.mask = slotMask;
    }


    async doInfiniteSpins() {

        if (this.isSpinning) return;
        this.isSpinning = true;
        let spinDoneCount = 0;

        this.reels.forEach(async reel => {

            await new Promise(resolve => setTimeout(resolve, reel.reelIndex * 250));

            await reel.spinReel();

            spinDoneCount++;
            if (spinDoneCount === this.reels.length) {
                spinDoneCount = 0;
                this.isSpinning = false;

                await new Promise(resolve => setTimeout(resolve, 1000));

                this.doInfiniteSpins();  // recursion
            }
        });
    }


    async doRandomSpin() {

        if (this.isSpinning) return;
        this.isSpinning = true;
        let spinDoneCount = 0,
            reelResults = [];

        this.reels.forEach(async reel => {

            await new Promise(resolve => setTimeout(resolve, reel.reelIndex * 250));

            await reel.spinReel();

            reelResults.push(reel.result);

            spinDoneCount++;
            if (spinDoneCount === this.reels.length) {
                spinDoneCount = 0;

                this.isSpinning = false;
                this.emit(this.EVENT.SPIN_COMPLETE, reelResults);
            }
        });
    }


    /**
     * Spin mock data
     * @param stopData
     */
    spinReels(stopData = []) {

        if (this.isSpinning) return;
        this.isSpinning = true;

        let reelResults = [],
            spinDoneCount = 0;

        this.reels.forEach((reel, index) => {

            setTimeout(async () => {
                    await reel.spinReel(stopData[index]);

                    reelResults.push(reel.result);

                    spinDoneCount++;
                    if (spinDoneCount === this.reels.length) {
                        spinDoneCount = 0;

                        this.isSpinning = false;
                        this.emit(this.EVENT.SPIN_COMPLETE, reelResults);
                    }
                },
                (index + 1) * REEL_START_DELAY
            );
        });
    }

}