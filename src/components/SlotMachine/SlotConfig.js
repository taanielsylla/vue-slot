export const SYMBOL_TYPE = {
    BAR: "Bar",
    BARx2: "2xBar",
    BARx3: "3xBar",
    CHERRY: "Cherry",
    SEVEN: "7",
};

export const STOP_POSITION = {
    TOP: 245,
    MIDDLE: 320,
    BOTTOM: 380,
};

export const ReelSymbolOrder = [
    SYMBOL_TYPE.BARx3,
    SYMBOL_TYPE.BAR,
    SYMBOL_TYPE.BARx2,
    SYMBOL_TYPE.SEVEN,
    SYMBOL_TYPE.CHERRY,
];

