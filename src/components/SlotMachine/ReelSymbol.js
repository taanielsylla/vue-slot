import * as PIXI from "pixi.js"
import AssetLoader from "../../core/AssetLoader/AssetLoader";

export default class ReelSymbol extends PIXI.Container {

    _orderIndex = 0;


    get orderIndex() {
        return this._orderIndex;
    }

    set orderIndex(value) {
        this._orderIndex = value;
    }

    get type() {
        return this._type;
    }

    get isMock() {
        return this._isMock;
    }

    constructor(type = "Bar", isMock = false) {
        super();

        this.mock = isMock;
        this._type = type;

        this.img = new PIXI.Sprite(AssetLoader.resources[type].texture);
        this.addChild(this.img);

        this.setSize(133);

        this._isMock = isMock;
    }

    setSize(size) {
        this.img.width = this.img.height = size;
    }


}