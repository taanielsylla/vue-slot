import * as PIXI from 'pixi.js'
import animate from 'gsap-promise';
import {Power0, Power2} from "gsap/TweenMax";
import {SYMBOL_TYPE, STOP_POSITION, ReelSymbolOrder} from "./SlotConfig";
import ReelSymbol from "./ReelSymbol";

const viewPortHeight = 270;


export default class Reel extends PIXI.Container {

    constructor(index) {
        super();

        this.reelIndex = index;

        this.symbols = [];
        this.reel = new PIXI.Container();
        this.addChild(this.reel);

        this.addSymbolsToReel();

        this.symbolHeight = this.symbols[0].height;
        this.reelHeight = this.symbolHeight * this.symbols.length;

        this.extraSpinPadding = 0;
    }


    addSymbolsToReel() {
        let symbol;
        ReelSymbolOrder.forEach(symbolType => {

            symbol = new ReelSymbol(symbolType);
            symbol.y = this.symbols.length * symbol.height;

            this.symbols.push(symbol);
            symbol.orderIndex = this.symbols.length - 1;

            this.reel.addChild(symbol);
        });
    }


    /**
     *
     * @param stopData - specify symbol & position where to stop reel at
     * @returns {Promise<number | *>}
     */
    async spinReel(stopData = {type: this.getRandomSymbolName(), position: this.getRandomSymbolPos()}) {

        this.stopData = stopData;

        return Promise.resolve()
            .then(() => {
                return this.bounceUp();
            })

            .then(() => {
                this.reel.filters = [new PIXI.filters.BlurFilter(2)];
                return this.middleSpin();
            })

            .then(() => {
                this.setFinalSymbols(); // set outcome symbols (server response goes here)
                return this.spinToFinalSymbols();
            })

            .then(() => {
                this.reel.filters = [];
                return this.bounceDown();
            })
    }


    bounceUp() {
        return animate.to(this.reel, .1, {
            y: this.reel.position.y - 15,
            ease: Power2.easeOut,
        })
            .then(() => {

                return animate.to(this.reel, .1, {
                    y: this.reel.position.y + 15,
                    ease: Power2.easeIn,
                })
            })
    }


    /**
     * The middle spin loops all the reel symbols x amount of time,
     * finally coming to an end in the same position where it started ( y = 0 )
     */
    middleSpin() {
        return animate.to(this.reel, 2, {
            y: this.reel.y + (this.reelHeight * 3) + this.extraSpinPadding,
            onUpdateScope: this,
            onUpdate: () => {
                this.moveSymbolToTop();
            },
            ease: Power0.easeNone,
        })
    }


    /**
     * Here we add 3 symbols on top of currently visible symbols, these "stop" symbols are the result of the spin.
     * The stop symbols keep their order which is defined in SlotConfig.ReelSymbolOrder
     */
    spinToFinalSymbols() {
        const stopPos = this.stopData.position;
        this.extraSpinPadding = ((this.reelHeight / 5) * 3) - stopPos; // height of 3 symbols

        const time = (this.stopData.position) * 0.0015;
        return animate.to(this.reel, time, {
            y: this.stopData.position,
            ease: Power0.easeNone,
        })
    }


    /**
     * Fancy animation to finish the spin
     * @returns {Q.Promise<any> | Q.Promise<T | never> | PromiseLike<T | never> | Promise<T | never> | *}
     */
    bounceDown() {
        return animate.to(this.reel, .1, {
            y: this.reel.position.y + 15,
            ease: Power2.easeOut,
        })
            .then(() => {
                return animate.to(this.reel, .1, {
                    y: this.reel.position.y - 15,
                    ease: Power2.easeIn,
                })
            })
    }


    /**
     * Move symbol from the bottom of the reel to top, create an illusion of an infinite scroller
     */
    moveSymbolToTop() {
        this.symbols.reverse().forEach(symbol => {
            const y = symbol.getGlobalPosition().y;

            if (y > viewPortHeight + this.symbolHeight) {

                if (symbol.isMock) { // remove old stop symbols
                    this.symbols.splice(this.symbols.indexOf(symbol), 1);
                    symbol.parent.removeChild(symbol);

                } else {
                    const topSymbol = this.getReelTopSymbol();
                    symbol.y = topSymbol.y - this.symbolHeight;
                }
            }
        });
    }


    /**
     * Attach round result symbols above current visible symbols of the reel.
     * These symbols will be tweened-in and later destroyed when tweened-out below the screen
     */
    setFinalSymbols() {
        this.resetPos();
        const topSymbol = this.getReelTopSymbol(),
            stopSymbolType = this.stopData.type,
            data = {position: this.stopData.position};

        // create symbols to show as final outcome & tween them in
        data.topSymbolType = ReelSymbolOrder[this.getNextSymbolIndex(ReelSymbolOrder.indexOf(stopSymbolType))];
        data.middleSymbolType = stopSymbolType;
        data.bottomSymbolType = ReelSymbolOrder[this.getPrevSymbolIndex(ReelSymbolOrder.indexOf(stopSymbolType))];

        const top = new ReelSymbol(data.topSymbolType, true),
            middle = new ReelSymbol(data.middleSymbolType, true),
            bottom = new ReelSymbol(data.bottomSymbolType, true);

        bottom.y = topSymbol.y - topSymbol.height;
        middle.y = bottom.y - topSymbol.height;
        top.y = middle.y - topSymbol.height;

        this.reel.addChild(top, middle, bottom);
        this.symbols.unshift(top, middle, bottom);

        this.parseResult(data);
    }


    getNextSymbolIndex(index) {
        return index - 1 < 0 ? ReelSymbolOrder.length - 1 : index - 1;
    }

    getPrevSymbolIndex(index) {
        return index + 1 > ReelSymbolOrder.length - 1 ? 0 : index + 1;
    }


    /**
     * Return the symbol that is currently positioned at the top of the reel
     * @returns {*}
     */
    getReelTopSymbol() {
        return this.symbols.reduce((prev, curr) => prev.y < curr.y ? prev : curr);
    }


    getRandomSymbolName() {
        switch (this.randomIntFromInterval(1, 5)) {
            case 1:
                return SYMBOL_TYPE.CHERRY;
            case 2:
                return SYMBOL_TYPE.SEVEN;
            case 3:
                return SYMBOL_TYPE.BARx2;
            case 4:
                return SYMBOL_TYPE.BAR;
            case 5:
                return SYMBOL_TYPE.BARx3;
        }
    }

    getRandomSymbolPos() {
        switch (this.randomIntFromInterval(1, 3)) {
            case 1:
                return STOP_POSITION.TOP;
            case 2:
                return STOP_POSITION.MIDDLE;
            case 3:
                return STOP_POSITION.BOTTOM;
        }
    }


    randomIntFromInterval(min, max) { // min and max included
        return Math.floor(Math.random() * (max - min + 1) + min);
    }

    resetPos() {
        this.reel.y = -this.extraSpinPadding;
        this.symbols.forEach(symbol => {
            symbol.y = symbol.orderIndex * symbol.height;
        });
    }

    parseResult(stopData) {
        if (stopData.position === STOP_POSITION.MIDDLE) this.result = {middle: stopData.middleSymbolType};
        else this.result = {top: stopData.topSymbolType, bottom: stopData.bottomSymbolType};
    }
}