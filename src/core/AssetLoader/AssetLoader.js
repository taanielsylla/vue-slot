import * as PIXI from 'pixi.js'

export default new class AssetLoader {

    _resources;
    _loader;


    get resources() {
        return this._resources;
    }


    async loadAssets() {

        let promiseResolve,
            p = new Promise(function (resolve) {
                promiseResolve = resolve;
            });

        PIXI.loader

            .add("Bar", require("../../assets/BAR.png"))
            .add("2xBar", require("../../assets/2xBAR.png"))
            .add("3xBar", require("../../assets/3xBAR.png"))
            .add("7", require("../../assets/7.png"))
            .add("Cherry", require("../../assets/Cherry.png"))
            .add("Spin", require("../../assets/spin.png"))

            .load((loader, resources) => {
                this._loader = loader;
                this._resources = resources;

                promiseResolve();
            });

        return p;
    }

}