import * as PIXI from "pixi.js"


export default class Button extends PIXI.Container {


    EVENT = {
        TAP: "TAP",
    };


    constructor(upTexture, overTexture = false, downTexture = false, disabledTexture = false) {
        super();


        this.upTexture = upTexture;
        this.overTexture = overTexture;
        this.downTexture = downTexture;
        this.disabledTexture = disabledTexture;


        this.image = new PIXI.Sprite(this.upTexture);
        this.image.interactive = true;
        this.image.buttonMode = true;
        this.image.anchor.set(.5);

        this.image.width = this.image.width * .25;
        this.image.height = this.image.height * .25;

        this.addChild(this.image);

        this.addListeners();

    }

    addListeners() {

        this.image.on("pointerdown", this.onDown, this);
        this.image.on("pointerup", this.onUp, this);
        this.image.on("pointerupoutside", this.onUp, this);
        this.image.on("pointertap", this.onTap, this);
    }

    onUp() {
        this.image.scale.set(this.image.scale.x + .05);
    }

    onDown() {
        this.image.scale.set(this.image.scale.x - .05);
    }

    onTap() {
        this.emit("TAP");
    }
}