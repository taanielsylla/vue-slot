import * as PIXI from 'pixi.js'
import AssetLoader from "./core/AssetLoader/AssetLoader";
import Game from "./components/Game";

export default class GameEntry {


    constructor() {
        console.log("Initialized game");

        window.addEventListener('load', async () => {
            console.log('Document loaded');

            await AssetLoader.loadAssets();
            this.init();
        })
    }


    init() {

        // The application will create a renderer using WebGL, if possible,
        // with a fallback to a canvas render. It will also setup the ticker
        // and the root stage PIXI.Container
        this.app = new PIXI.Application({
            width: 400,
            height: 550,
            antialias: true,
            resolution: 2,
        });

        this.app.renderer.backgroundColor = 0x061639;

        // The application will create a canvas element for you that you
        // can then insert into the DOM
        let gameContainer = document.getElementById("gameContainer");
        gameContainer.appendChild(this.app.view);


        const game = new Game(this.app);
        this.app.stage.addChild(game);
    }

}